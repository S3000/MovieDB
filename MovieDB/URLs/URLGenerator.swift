//
//  URLGenerator.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import Foundation
class URLGenerator {
    let ROOT = "https://api.themoviedb.org/3/search/movie?api_key="
    let photoBaseUrl = "http://image.tmdb.org/t/p/w185"
    let apiKey = "24c545bd482432ee86c7ffd343089d31"
    func returnSearchUrl (movieName:String,pageNumber:Int) -> String{
        let pageNumberString = String(describing:pageNumber)
        let originalString = ROOT + apiKey + "&query=" + movieName + "&page=" + pageNumberString
        let urlString = originalString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        return urlString!
    }
    func returnPhotoUrl (path:String) -> String {
        return photoBaseUrl + path
    }
}
