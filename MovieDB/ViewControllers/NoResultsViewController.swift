//
//  NoResultsViewController.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import Foundation
import UIKit
class NoResultsViewController:UIViewController {
    @IBOutlet weak var customView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBOutlet var dissmiss: UITapGestureRecognizer!
    @IBAction func dismiss(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.6, animations: {
            self.customView.frame.origin.y =  self.customView.frame.origin.y - 500
            self.customView.frame.origin.x = self.customView.frame.origin.x - 500
            self.view.backgroundColor?.withAlphaComponent(0)
        },completion:{ (finished) in
            
            if finished {
                self.dismiss(animated: true, completion: nil)
            }
        })
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        customView.alpha = 0
        customView.frame.origin.y = self.customView.frame.origin.y - 100
        UIView.animate(withDuration: 0.6, animations: {
            self.customView.alpha = 1
            self.customView.frame.origin.y = self.customView.frame.origin.y + 100
        })
    }
}
