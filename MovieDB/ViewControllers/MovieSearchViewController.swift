//
//  MovieSearchViewController.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import UIKit
import Kingfisher
import SVProgressHUD
var movieSearch = MovieSearchViewController()
class MovieSearchViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UISearchBarDelegate,UIScrollViewDelegate {
    //MARK:Outlets
    @IBOutlet weak var recentView: UIView!
    @IBOutlet weak var searchMovies: UISearchBar!
    @IBOutlet weak var resultCollectionView: UICollectionView!
    @IBOutlet weak var DetailsView: UIView!
    @IBOutlet var showRecentSearches: UITapGestureRecognizer!
    //MARK:Variables
    var searchResults:[SearchResultModal] = []
    var isDetailsViewShown:Bool = false
    var recentSearches:[String] = []
    var totalPages:Int = 0
    var currentPage:Int = 0
    var searchSyntax:String = ""
    //MARK:Setting up collection view
    var isLoading: Bool = false
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if self.currentPage == 1{
            self.currentPage = 2
        }
        let contentOffsetX = scrollView.contentOffset.x
        if contentOffsetX >= (scrollView.contentSize.width - scrollView.bounds.width) - 20 /* Needed offset */ {
            guard !self.isLoading else { return }
            self.isLoading = true
            // load more data
            if self.currentPage < totalPages || self.currentPage == totalPages {
                
                print(searchSyntax)
                SVProgressHUD.show()
                print(self.currentPage)
                if self.isLoading == true {
                    
                    getResults(searchQuery: searchSyntax, pageNumber: self.currentPage) { (modal, resultmodal) in
                        self.searchResults.removeAll()
                        self.searchResults = modal
                        if modal.isEmpty == false {
                            if let totalResult = resultmodal.totalResult {
                                self.totalPages = totalResult
                                print(self.totalPages)
                                
                                self.currentPage += 1
                            }
                        }
                        
                        
                    }
                    
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    SVProgressHUD.dismiss()
                    
                    self.resultCollectionView.reloadData()
                    self.resultCollectionView.scrollToItem(at: IndexPath(row: 0, section: 0),
                                                           at: .right,
                                                           animated: true)
                }
                
                // than set self.isLoading to false when new data is loaded
                self.isLoading = false
            }else{
                
                self.performSegue(withIdentifier: "noResults", sender: self)
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.DetailsView.isHidden = false
        self.DetailsView.alpha = 1
        if isDetailsViewShown == false {
            UIView.animate(withDuration: 0.8, animations:{
                self.DetailsView.frame.origin.y =  self.DetailsView.frame.minY/2 - 120
            })
            isDetailsViewShown = true
        }
        
        detailsContainer.setData(detailsModal: self.searchResults[indexPath.row])
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searchResults.count > 0 {
            resultCollectionView.alpha = 1
        }else{
            resultCollectionView.alpha = 0.8
        }
        return searchResults.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCell", for: indexPath) as! ResultCollectionViewCell
        if let title = searchResults[indexPath.row].title {
            cell.titleLabel.text = title
        }
        if let poster = searchResults[indexPath.row].posterPath {
            let urlString = URLGenerator().returnPhotoUrl(path:poster)
            let url = URL(string: urlString)
            cell.coverImage.kf.setImage(with: url)
        }
        
        return cell
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        if self.recentSearches.isEmpty == false {
            self.recentView.isHidden = false
            recentSearchesController.reloadSearchers(searchData: self.recentSearches)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.currentPage = 0
        if let searchText = searchBar.text {
            searchSyntax = searchText
            var shouldAddThis:Bool = true
            for each in recentSearches {
                if each == searchText {
                    shouldAddThis = false
                }
            }
            if shouldAddThis == true {
                recentSearches.append(searchText)
                UserDefaults.standard.set(recentSearches, forKey: "recentSearches")
            }
            self.callForApiGetData(searchQuery: searchText)
        }
    }
    func callForApiGetData (searchQuery:String){
        self.currentPage = 1
        recentView.isHidden = true
        self.view.endEditing(true)
        getResults(searchQuery: searchQuery,pageNumber: self.currentPage) { (moviemodal,resultmodal) in
            if moviemodal.isEmpty == false {
                self.searchResults = moviemodal
                self.resultCollectionView.reloadData()
            }
            if let totalResult = resultmodal.totalResult {
                self.totalPages = totalResult
                if resultmodal.totalResult == 0 {
                    self.performSegue(withIdentifier: "noResults", sender: self)
                }
                if let current = resultmodal.page {
                    self.currentPage = current
                }
            }
        }
    }
    func dismissDetails() {
        UIView.animate(withDuration: 0.8, animations: {
            self.DetailsView.frame.origin.y = self.view.frame.maxY + 120
            
        })
        isDetailsViewShown = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let recentSearchersNotSet = UserDefaults.standard.object(forKey: "recentSearches") as? [String]{
            self.recentSearches = recentSearchersNotSet
            for each in recentSearches {
                print (each)
            }
        }
        movieSearch = self
        
        
        
    }
    @IBAction func showRecents(_ sender: UITapGestureRecognizer) {
        self.recentView.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
       self.DetailsView.frame.origin.y = self.DetailsView.frame.maxY - 120
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


