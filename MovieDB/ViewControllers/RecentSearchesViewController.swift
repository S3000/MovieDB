//
//  RecentSearchesViewController.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

//
//  RecentSearchesViewController.swift
//  MovieSearch
//
//  Created by Farbod on 7/3/18.
//  Copyright © 2018 Farbod rahiminik. All rights reserved.
//

import UIKit
var recentSearchesController = RecentSearchesViewController()
class RecentSearchesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK:TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearches.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        movieSearch.callForApiGetData(searchQuery: recentSearches[indexPath.row])
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        cell.textLabel?.text = recentSearches[indexPath.row]
        cell.textLabel?.textColor = UIColor.white
        return cell
    }
    //MARK:Varriables
    var recentSearches:[String] = []
    //MARK:Outlets
    @IBOutlet weak var recentTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recentSearchesController = self
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.view.backgroundColor = .clear
            let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.view.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.view.insertSubview(blurEffectView, at: 0)
        } else {
            view.backgroundColor = .black
        }
        
    }
    func reloadSearchers(searchData:[String]) {
        self.recentSearches = searchData
        self.recentTableView.reloadData()
    }
    
}
