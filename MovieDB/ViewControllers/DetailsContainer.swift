//
//  DetailsContainer.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import UIKit
var detailsContainer = DetailsContainer()
class DetailsContainer: UIViewController {
    //MARK:Outlets
    
    @IBOutlet weak var star5: UIImageView!
    @IBOutlet weak var star4: UIImageView!
    @IBOutlet weak var star3: UIImageView!
    @IBOutlet weak var star2: UIImageView!
    @IBOutlet weak var star1: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overViewTextField: UITextView!
    @IBOutlet weak var poster: UIImageView!
    //MARK:Variables
    var singleMovie = SearchResultModal()
    override func viewDidLoad() {
        super.viewDidLoad()
        overViewTextField.isEditable = false
        detailsContainer = self
        
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            self.poster.backgroundColor = .clear
            let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.poster.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.poster.insertSubview(blurEffectView, at: 0)
        } else {
            view.backgroundColor = .black
        }
    }
    func setData (detailsModal:SearchResultModal){
        titleLabel.text = detailsModal.title
        overViewTextField.text = detailsModal.overView
        if detailsModal.posterPath != nil{
            let posterPath = detailsModal.posterPath
            let posterStringUrl = URLGenerator().returnPhotoUrl(path: posterPath!)
            let posterUrl = URL(string: posterStringUrl)
            poster?.kf.setImage(with: posterUrl!)
        }
        let ratingInt = Int(truncating: detailsModal.voteAverage!)
        switch ratingInt/2 {
        case 1:
            star1.isHidden = false
            star2.isHidden = true
            star3.isHidden = true
            star4.isHidden = true
            star5.isHidden = true
        case 2:
            star1.isHidden = false
            star2.isHidden = false
            star3.isHidden = true
            star4.isHidden = true
            star5.isHidden = true
        case 3:
            star1.isHidden = false
            star2.isHidden = false
            star3.isHidden = false
            star4.isHidden = true
            star5.isHidden = true
        case 4:
            star1.isHidden = false
            star2.isHidden = false
            star3.isHidden = false
            star4.isHidden = false
            star5.isHidden = true
        case 5:
            star1.isHidden = false
            star2.isHidden = false
            star3.isHidden = false
            star4.isHidden = false
            star5.isHidden = false
        default:
            break
        }
    }
    @IBAction func Dismiss(_ sender: UIButton) {
        movieSearch.dismissDetails()
        
    }
    
}

