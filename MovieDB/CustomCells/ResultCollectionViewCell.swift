//
//  ResultCollectionViewCell.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import UIKit

class ResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
}
