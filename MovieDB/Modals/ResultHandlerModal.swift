//
//  ResultHandlerModal.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import Foundation
class ResultHandlerModal {
    private var _page:Int?
    var page:Int?{
        get {return _page}
        set (new){_page = new}
    }
    private var _totalResults:Int?
    var totalResult:Int?{
        get {return _totalResults}
        set (new){_totalResults = new}
    }
}
