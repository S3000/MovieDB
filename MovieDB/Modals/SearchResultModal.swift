//
//  SearchResultModal.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import Foundation
class SearchResultModal {
    private var _voteCount:NSNumber?
    var voteCount:NSNumber?{
        get {return _voteCount}
        set (new){_voteCount = new}
    }
    private var _id:NSNumber?
    var id:NSNumber?{
        get {return _id}
        set (new){_id = new}
    }
    private var _video:Bool?
    var video:Bool?{
        get {return _video}
        set (new){_video = new}
    }
    private var _voteAverage:NSNumber?
    var voteAverage:NSNumber?{
        get {return _voteAverage}
        set (new){_voteAverage = new}
    }
    private var _title:String?
    var title:String? {
        get {return _title}
        set (new){_title = new}
    }
    private var _popularity:NSNumber?
    var popularity:NSNumber?{
        get{return _popularity}
        set (new) {_popularity = new}
    }
    private var _posterPath:String?
    var posterPath:String?{
        get {return _posterPath}
        set (new){_posterPath = new}
    }
    private var _originalLanguage:String?
    var originalLanguage:String?{
        get {return _originalLanguage}
        set (new){_originalLanguage = new}
    }
    private var _originalTitle:String?
    var originalTitle:String?{
        get {return _originalLanguage}
        set (new){_originalLanguage = new}
    }
    private var _genreIds:[NSNumber]?
    var genreIds:[NSNumber]?{
        get {return _genreIds}
        set (new){_genreIds = new}
    }
    private var _backDropPath:String?
    var backDropPath:String?{
        get {return _backDropPath}
        set (new){_backDropPath = new}
    }
    private var _adult:Bool?
    var adult:Bool?{
        get {return _adult}
        set (new){_adult = new}
    }
    private var _overview:String?
    var overView:String?{
        get {return _overview}
        set (new){_overview = new}
    }
    private var _releaseDate:String?
    var releaseDate:String?{
        get {return _releaseDate}
        set (new){_releaseDate = new}
    }
}
