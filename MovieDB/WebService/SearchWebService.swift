//
//  SearchWebService.swift
//  MovieDB
//
//  Created by Soroush Fathi on 7/5/18.
//  Copyright © 2018 Soroush Fathi. All rights reserved.
//

import Foundation
import Alamofire
func getResults (searchQuery:String,pageNumber:Int,completion:@escaping (([SearchResultModal],ResultHandlerModal) -> ())){
    let searchUrl = URLGenerator().returnSearchUrl(movieName: searchQuery,pageNumber: pageNumber)
    print(searchUrl)
    let resultHandle = ResultHandlerModal()
    Alamofire.request(searchUrl, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).validate().responseJSON{(response) in
        var moviesFinalModal:[SearchResultModal] = []
        let responseJson = response.result.value as? [String:Any]
        if let responseJsonUnwrapped = responseJson {
            resultHandle.page = responseJsonUnwrapped["page"] as? Int
            resultHandle.totalResult = responseJsonUnwrapped["total_pages"] as? Int
            let results = responseJsonUnwrapped["results"] as? [NSDictionary]
            if let resultsUnwrapped = results {
                for each in resultsUnwrapped {
                    let movieSingleModal = SearchResultModal()
                    movieSingleModal.voteCount = each["vote_count"] as? NSNumber
                    movieSingleModal.id = each ["id"] as? NSNumber
                    movieSingleModal.video = each ["video"] as? Bool
                    movieSingleModal.voteAverage = each["vote_average"] as? NSNumber
                    movieSingleModal.title = each["title"] as? String
                    movieSingleModal.popularity = each["popularity"] as? NSNumber
                    movieSingleModal.posterPath = each["poster_path"] as? String
                    movieSingleModal.originalLanguage = each ["original_language"] as? String
                    movieSingleModal.originalTitle = each ["original_title"] as? String
                    movieSingleModal.genreIds = each ["genre_ids"] as? [NSNumber]
                    movieSingleModal.backDropPath = each["backdrop_path"] as? String
                    movieSingleModal.adult = each ["adult"] as? Bool
                    movieSingleModal.overView = each["overview"] as? String
                    movieSingleModal.releaseDate = each ["release_date"] as? String
                    moviesFinalModal.append(movieSingleModal)
                }
            }
        }
        completion(moviesFinalModal,resultHandle)
    }
}
